﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ButtonHandler : MonoBehaviour {
    public Button phase1Button;
    public Button phase2Button;
    public Button phase3Button;
    public InputField subjectName;

	// Use this for initialization
	void Start () {
        phase1Button.GetComponent<Button>().onClick.AddListener(loadPhase1);
        phase2Button.GetComponent<Button>().onClick.AddListener(loadPhase2);
        phase3Button.GetComponent<Button>().onClick.AddListener(loadPhase3);
	}

    void loadPhase1()
    {
        GameMemory.phase = 1;
        GameMemory.subjectName = subjectName.transform.GetChild(2).GetComponent<Text>().text;
        SceneManager.LoadScene("Scenes/scene1", LoadSceneMode.Single);
    }

    void loadPhase2()
    {
        GameMemory.phase = 2;
        GameMemory.subjectName = subjectName.transform.GetChild(2).GetComponent<Text>().text;
        SceneManager.LoadScene("Scenes/scene1", LoadSceneMode.Single);
    }

    void loadPhase3()
    {
        GameMemory.phase = 3;
        GameMemory.subjectName = subjectName.transform.GetChild(2).GetComponent<Text>().text;
        SceneManager.LoadScene("Scenes/scene1", LoadSceneMode.Single);
    }
}
