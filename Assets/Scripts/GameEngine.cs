﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameEngine : MonoBehaviour
{
    public GameObject target;
    public GameObject winText;
    public GameObject loadSceneText;
    public int number;
    public float ballRadius;
    public GameObject guideText;
    public int stage;
    public GameObject errorSound;
    private int currentState;
    private bool start;
    private int error;
    private String lastValue;
    private Scene scene;
    private StreamWriter file;
    private float timer;
    private AudioSource audio;

    void Start()
    {
        GameBuilder.number = number;
        GameBuilder.target = target;
        GameBuilder.ballRadius = ballRadius;
        GameBuilder.stage = stage;
        GameBuilder.Build();
        currentState = -1;
        start = false;
        error = 0;
        lastValue = "";
        scene = SceneManager.GetActiveScene();
        audio = errorSound.GetComponent<AudioSource>();
        Debug.Log(GameMemory.phase);
        


        if (scene.name.Equals("scene1"))
        {
            GameMemory.timeNow = DateTime.Now.ToString("yyyyMMddHHmmssffff");
            file = new StreamWriter(new FileStream(GameMemory.subjectName + GameMemory.phase + "_" +  GameMemory.timeNow + ".csv", FileMode.Create, FileAccess.ReadWrite));
            file.WriteLine("Phase," + GameMemory.phase);
            file.WriteLine("");
            file.WriteLine("");
        } else
        {
            file = new StreamWriter(new FileStream(GameMemory.subjectName + GameMemory.phase + "_" + GameMemory.timeNow + ".csv", FileMode.Append, FileAccess.Write));
        }
        file.WriteLine("Stage," + stage);
        file.WriteLine("Number of Balls," + number);
        file.WriteLine("");
        file.WriteLine("");
        file.WriteLine("Expected,Selected");
    }


    void Update()
    {
        if (start)
        {
            timer += Time.deltaTime;
            Vector3 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            GameObject ball = Clicked(mousePosition);

            if (ball != null)
            {
                Component textMesh = ball.transform.GetChild(0).GetComponent("TextMesh");
                String value = textMesh.GetComponent<TextMesh>().text;
                if (value.Equals(NextValue()))
                {
                    if (currentState == number - 2)
                    {
                        Destroy(guideText);
                        winText = Instantiate(winText);
                        file.WriteLine("");
                        file.WriteLine("");
                        file.WriteLine("Level Time," + timer);
                        file.WriteLine("Level Errors," + error);
                        file.WriteLine("");
                        file.WriteLine("");
                        file.Flush();
                        file.Close();
                        Debug.Log(error);
                        Invoke("LoadSceneText", 2);
                        Invoke("ChangeScene", 4);
                    }
                    currentState += 1;
                    guideText.GetComponent<TextMesh>().text = "Move your mouse to " + NextValue();
                    lastValue = value;
                }
                else
                {
                    if (!lastValue.Equals(value))
                    {
                        error += 1;
                        audio.Play();
                        lastValue = value;
                        file.WriteLine(NextValue() + "," + lastValue);
                    }
                }
            }
        }
        else if (Input.GetMouseButton(0))
        {
            Vector3 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            GameObject ball = Clicked(mousePosition);

            if (ball != null)
            {
                Component textMesh = ball.transform.GetChild(0).GetComponent("TextMesh");
                String value = textMesh.GetComponent<TextMesh>().text;
                if (value.Equals(NextValue()))
                {
                    start = true;
                    timer = 0.0f;
                    currentState = 0;
                    guideText.GetComponent<TextMesh>().text = "Move your mouse to " + NextValue();
                    lastValue = value;
                }
                else
                {
                    if (!lastValue.Equals(value))
                    {
                        error += 1;
                        audio.Play();
                        lastValue = value;
                        file.WriteLine(NextValue() + "," + lastValue);
                    }

                }
            }

        }

    }

    private void ChangeScene()
    {
        if(scene.name.Equals("scene1"))
        {
            SceneManager.LoadScene("Scenes/scene2", LoadSceneMode.Single);
        } else if(scene.name.Equals("scene2"))
        {
            SceneManager.LoadScene("Scenes/scene3", LoadSceneMode.Single);
        } else if(scene.name.Equals("scene3"))
        {
            SceneManager.LoadScene("Scenes/scene4", LoadSceneMode.Single);
        } else
        {
            Application.Quit();
        }
    }


    private void LoadSceneText()
    {
        foreach (GameObject ball in GameMemory.balls)
        {
            Destroy(ball);
        }
        Destroy(winText);
        if (scene.name.Equals("scene1")) {
            loadSceneText.GetComponent<TextMesh>().text = "Loading Level 2";
        }
        else if (scene.name.Equals("scene2"))
        {
            loadSceneText.GetComponent<TextMesh>().text = "Loading Level 3";
        }
        else if (scene.name.Equals("scene3"))
        {
            loadSceneText.GetComponent<TextMesh>().text = "Loading Level 4";
        }
        else
        {
            loadSceneText.GetComponent<TextMesh>().text = "Game Successfully Completed!";
        }
        Instantiate(loadSceneText);
    }

    private String NextValue()
    {
        char st = 'A';
        if(stage == 1)
        {
            return "" + (currentState + 2);
        } else if(stage == 2)
        {
            int temp = currentState + 2;
            int val = temp / 2;
            if (temp % 2 == 0)
            {
                char curr = (char)(Convert.ToUInt16(st) + (val - 1));
                return "" + curr;
            } else
            {
                return "" + (val+1);
            }
        }
        return "";
    }


    private GameObject Clicked(Vector3 mousePosition)
    {
        for (int i = 0; i < number; i++)
        {
            if (Vector3.Distance(GameMemory.centers[i], mousePosition) <= ballRadius)
            {
                return GameMemory.balls[i];
            }
        }
        return null;
    }


}
