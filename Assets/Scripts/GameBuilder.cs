﻿using System.Collections;using System.Collections.Generic;using UnityEngine;using System;public class GameBuilder : MonoBehaviour {    public static GameObject target;    public static int number;    public static float ballRadius;    public static int stage;		public static void Build()    {        char st = 'A';        GameMemory.balls = new GameObject[number];        GenerateCenters();        for(int i = 0; i < number; i++)        {            Vector3 center = GameMemory.centers[i];            GameObject ball = Instantiate(target, center, Quaternion.identity);            GameMemory.balls[i] = ball;            Component textMesh = ball.transform.GetChild(0).GetComponent("TextMesh");            if(stage == 1)            {                textMesh.GetComponent<TextMesh>().text = "" + (i + 1);            } else if (stage == 2)            {                int val = i / 2;                if (i % 2 == 0)                {                    textMesh.GetComponent<TextMesh>().text = "" + (val + 1);                } else                {                    char cur = (char)(Convert.ToUInt16(st) + val);                    textMesh.GetComponent<TextMesh>().text = "" + (cur);                }            }        }    }    private static bool IsAvailable(Vector3 center, int tillNow)     {        for(int i = 0; i < tillNow; i++)        {            if(Vector3.Distance(center, GameMemory.centers[i]) <= 2*ballRadius)            {                return false;            }        }        return true;    }    private static void GenerateCenters()
    {
        if(GameMemory.phase == 1)
        {
            if(stage == 1 && number == 5)
            {
                GameMemory.centers = new Vector3[] {
                    new Vector3(20.69f, 19.68f, 0.0f),
                    new Vector3(-33.43f, 25.46f, 0.0f),
                    new Vector3(12.88f, -26.50f, 0.0f),
                    new Vector3(36.68f, -7.99f, 0.0f),
                    new Vector3(-10.56f, 28.87f, 0.0f)
                };
            } else if(stage == 1 && number == 25)
            {
                GameMemory.centers = new Vector3[] {
                    new Vector3(-3.17f, 36.09f, 0.0f),
                    new Vector3(-25.59f, -10.71f, 0.0f),
                    new Vector3(-22.65f, -35.16f, 0.0f),
                    new Vector3(17.05f, 10.85f, 0.0f),
                    new Vector3(-30.55f, 20.52f, 0.0f),
                    new Vector3(23.03f, -10.44f, 0.0f),
                    new Vector3(-7.68f, 11.61f, 0.0f),
                    new Vector3(31.65f, 25.33f, 0.0f),
                    new Vector3(11.81f, -5.16f, 0.0f),
                    new Vector3(-29.60f, -25.03f, 0.0f),
                    new Vector3(-1.96f, -16.21f, 0.0f),
                    new Vector3(20.87f, 36.83f, 0.0f),
                    new Vector3(-2.34f, -7.83f, 0.0f),
                    new Vector3(-23.95f, -0.19f, 0.0f),
                    new Vector3(-13.56f, 29.61f, 0.0f),
                    new Vector3(36.32f, 5.90f, 0.0f),
                    new Vector3(23.72f, 23.83f, 0.0f),
                    new Vector3(-16.33f, -7.45f, 0.0f),
                    new Vector3(36.91f, -27.72f, 0.0f),
                    new Vector3(22.96f, -20.11f, 0.0f),
                    new Vector3(1.70f, -32.03f, 0.0f),
                    new Vector3(-21.56f, 36.83f, 0.0f),
                    new Vector3(25.48f, 3.14f, 0.0f),
                    new Vector3(8.44f, 6.29f, 0.0f),
                    new Vector3(-2.21f, 4.31f, 0.0f)
                };
            } else if(stage == 2 && number == 5)
            {
                GameMemory.centers = new Vector3[] {
                    new Vector3(24.51f, -32.18f, 0.0f),
                    new Vector3(-11.61f, 14.18f, 0.0f),
                    new Vector3(1.17f, -23.20f, 0.0f),
                    new Vector3(4.27f, 16.12f, 0.0f),
                    new Vector3(1.81f, -31.27f, 0.0f)
                };
            } else if(stage == 2 && number == 25)
            {
                GameMemory.centers = new Vector3[] {
                    new Vector3(-35.92f, 12.32f, 0.0f),
                    new Vector3(-10.03f, -29.74f, 0.0f),
                    new Vector3(-5.75f, 24.31f, 0.0f),
                    new Vector3(34.77f, 31.16f, 0.0f),
                    new Vector3(34.81f, -32.60f, 0.0f),
                    new Vector3(36.40f, 2.81f, 0.0f),
                    new Vector3(-28.91f, -30.09f, 0.0f),
                    new Vector3(-24.46f, 8.76f, 0.0f),
                    new Vector3(21.81f, -25.27f, 0.0f),
                    new Vector3(-9.15f, 11.31f, 0.0f),
                    new Vector3(7.33f, 3.41f, 0.0f),
                    new Vector3(35.40f, 19.82f, 0.0f),
                    new Vector3(18.69f, 4.51f, 0.0f),
                    new Vector3(-18.26f, -14.50f, 0.0f),
                    new Vector3(24.04f, 33.92f, 0.0f),
                    new Vector3(7.78f, 35.49f, 0.0f),
                    new Vector3(-12.07f, -8.88f, 0.0f),
                    new Vector3(5.58f, 27.54f, 0.0f),
                    new Vector3(23.70f, 16.44f, 0.0f),
                    new Vector3(0.25f, -15.80f, 0.0f),
                    new Vector3(9.38f, -7.82f, 0.0f),
                    new Vector3(24.40f, -1.25f, 0.0f),
                    new Vector3(13.72f, -34.93f, 0.0f),
                    new Vector3(29.24f, -14.39f, 0.0f),
                    new Vector3(-31.43f, -13.56f, 0.0f)
                };
            }
        } else if(GameMemory.phase == 2)
        {
            if (stage == 1 && number == 5)
            {
                GameMemory.centers = new Vector3[] {
                    new Vector3(0.63f, -22.91f, 0.0f),
                    new Vector3(8.75f, -17.82f, 0.0f),
                    new Vector3(-10.83f, -15.88f, 0.0f),
                    new Vector3(8.27f, 28.72f, 0.0f),
                    new Vector3(7.28f, -31.41f, 0.0f)
                };
            }
            else if (stage == 1 && number == 25)
            {
                GameMemory.centers = new Vector3[] {
                    new Vector3(30.52f, 13.71f, 0.0f),
                    new Vector3(-5.35f, -1.89f, 0.0f),
                    new Vector3(-0.03f, 28.55f, 0.0f),
                    new Vector3(22.28f, 5.03f, 0.0f),
                    new Vector3(30.93f, 23.21f, 0.0f),
                    new Vector3(12.26f, -29.87f, 0.0f),
                    new Vector3(31.97f, -34.15f, 0.0f),
                    new Vector3(-34.13f, -19.92f, 0.0f),
                    new Vector3(-24.76f, -18.14f, 0.0f),
                    new Vector3(-14.89f, -6.19f, 0.0f),
                    new Vector3(0.42f, -32.96f, 0.0f),
                    new Vector3(-9.65f, 34.78f, 0.0f),
                    new Vector3(2.52f, -16.82f, 0.0f),
                    new Vector3(-25.18f, 11.84f, 0.0f),
                    new Vector3(-20.60f, 30.17f, 0.0f),
                    new Vector3(5.01f, 8.68f, 0.0f),
                    new Vector3(-16.57f, -36.34f, 0.0f),
                    new Vector3(31.86f, 2.51f, 0.0f),
                    new Vector3(11.54f, 28.13f, 0.0f),
                    new Vector3(-32.45f, 34.93f, 0.0f),
                    new Vector3(26.61f, -23.72f, 0.0f),
                    new Vector3(20.96f, 19.11f, 0.0f),
                    new Vector3(-29.31f, 1.40f, 0.0f),
                    new Vector3(-28.71f, 22.83f, 0.0f),
                    new Vector3(22.32f, -35.96f, 0.0f)
                };
            }
            else if (stage == 2 && number == 5)
            {
                GameMemory.centers = new Vector3[] {
                    new Vector3(-34.39f, -6.11f, 0.0f),
                    new Vector3(-8.70f, 28.49f, 0.0f),
                    new Vector3(1.36f, 2.81f, 0.0f),
                    new Vector3(-10.88f, -4.63f, 0.0f),
                    new Vector3(-25.49f, -27.39f, 0.0f)
                };
            }
            else if (stage == 2 && number == 25)
            {
                GameMemory.centers = new Vector3[] {
                    new Vector3(15.06f, -10.58f, 0.0f),
                    new Vector3(36.75f, 16.63f, 0.0f),
                    new Vector3(-3.29f, 21.10f, 0.0f),
                    new Vector3(-15.52f, -27.82f, 0.0f),
                    new Vector3(18.43f, -22.92f, 0.0f),
                    new Vector3(29.80f, -14.10f, 0.0f),
                    new Vector3(-1.90f, -20.99f, 0.0f),
                    new Vector3(24.05f, 36.87f, 0.0f),
                    new Vector3(-25.51f, -14.23f, 0.0f),
                    new Vector3(-1.98f, -4.05f, 0.0f),
                    new Vector3(-12.74f, -15.10f, 0.0f),
                    new Vector3(-21.78f, -22.79f, 0.0f),
                    new Vector3(5.66f, 34.84f, 0.0f),
                    new Vector3(22.65f, 15.27f, 0.0f),
                    new Vector3(1.33f, 10.12f, 0.0f),
                    new Vector3(-16.38f, 6.92f, 0.0f),
                    new Vector3(31.02f, 24.15f, 0.0f),
                    new Vector3(-3.10f, -32.35f, 0.0f),
                    new Vector3(-15.35f, -2.75f, 0.0f),
                    new Vector3(35.98f, -5.57f, 0.0f),
                    new Vector3(-34.20f, 25.14f, 0.0f),
                    new Vector3(-23.33f, 19.51f, 0.0f),
                    new Vector3(12.09f, -31.08f, 0.0f),
                    new Vector3(-32.37f, -19.66f, 0.0f),
                    new Vector3(2.58f, -13.60f, 0.0f)
                };
            }
        } else if(GameMemory.phase == 3)
        {
            if (stage == 1 && number == 5)
            {
                GameMemory.centers = new Vector3[] {
                    new Vector3(1.84f, 19.62f, 0.0f),
                    new Vector3(26.41f, 32.90f, 0.0f),
                    new Vector3(6.31f, 6.79f, 0.0f),
                    new Vector3(3.88f, -34.50f, 0.0f),
                    new Vector3(24.44f, 22.56f, 0.0f)
                };
            }
            else if (stage == 1 && number == 25)
            {
                GameMemory.centers = new Vector3[] {
                    new Vector3(23.28f, 3.05f, 0.0f),
                    new Vector3(-32.48f, -30.23f, 0.0f),
                    new Vector3(-24.58f, 33.67f, 0.0f),
                    new Vector3(7.30f, -21.39f, 0.0f),
                    new Vector3(-20.22f, -25.34f, 0.0f),
                    new Vector3(31.74f, 30.63f, 0.0f),
                    new Vector3(-26.75f, -17.02f, 0.0f),
                    new Vector3(-2.67f, -8.74f, 0.0f),
                    new Vector3(-5.65f, -20.47f, 0.0f),
                    new Vector3(9.79f, 14.50f, 0.0f),
                    new Vector3(-22.24f, -7.99f, 0.0f),
                    new Vector3(29.70f, -24.63f, 0.0f),
                    new Vector3(-13.33f, 9.30f, 0.0f),
                    new Vector3(-8.09f, 2.09f, 0.0f),
                    new Vector3(-5.08f, 26.26f, 0.0f),
                    new Vector3(1.07f, 20.41f, 0.0f),
                    new Vector3(11.78f, 24.99f, 0.0f),
                    new Vector3(-11.06f, -28.59f, 0.0f),
                    new Vector3(11.56f, -9.50f, 0.0f),
                    new Vector3(35.13f, 3.09f, 0.0f),
                    new Vector3(-33.05f, 13.44f, 0.0f),
                    new Vector3(22.31f, 11.37f, 0.0f),
                    new Vector3(9.44f, 6.33f, 0.0f),
                    new Vector3(21.46f, -6.43f, 0.0f),
                    new Vector3(2.40f, -34.85f, 0.0f)
                };
            }
            else if (stage == 2 && number == 5)
            {
                GameMemory.centers = new Vector3[] {
                    new Vector3(-8.87f, 16.09f, 0.0f),
                    new Vector3(2.48f, 21.39f, 0.0f),
                    new Vector3(8.12f, 31.98f, 0.0f),
                    new Vector3(-24.49f, 1.20f, 0.0f),
                    new Vector3(17.21f, 19.75f, 0.0f)
                };
            }
            else if (stage == 2 && number == 25)
            {
                GameMemory.centers = new Vector3[] {
                    new Vector3(23.42f, -10.80f, 0.0f),
                    new Vector3(-36.19f, 30.94f, 0.0f),
                    new Vector3(5.26f, 17.62f, 0.0f),
                    new Vector3(22.06f, -21.27f, 0.0f),
                    new Vector3(-27.45f, -27.90f, 0.0f),
                    new Vector3(4.67f, -34.54f, 0.0f),
                    new Vector3(14.25f, -17.05f, 0.0f),
                    new Vector3(6.95f, -5.61f, 0.0f),
                    new Vector3(-6.41f, 13.98f, 0.0f),
                    new Vector3(-27.18f, 19.15f, 0.0f),
                    new Vector3(11.32f, 31.52f, 0.0f),
                    new Vector3(32.83f, 30.67f, 0.0f),
                    new Vector3(-19.64f, 10.17f, 0.0f),
                    new Vector3(-13.58f, 4.94f, 0.0f),
                    new Vector3(-14.56f, 18.97f, 0.0f),
                    new Vector3(-36.38f, 12.88f, 0.0f),
                    new Vector3(-24.08f, -36.69f, 0.0f),
                    new Vector3(-6.52f, -19.80f, 0.0f),
                    new Vector3(-3.21f, 30.16f, 0.0f),
                    new Vector3(4.26f, 7.39f, 0.0f),
                    new Vector3(27.96f, -32.70f, 0.0f),
                    new Vector3(-25.86f, 28.22f, 0.0f),
                    new Vector3(6.90f, -15.46f, 0.0f),
                    new Vector3(-31.17f, 5.35f, 0.0f),
                    new Vector3(34.07f, 5.32f, 0.0f)
                };
            }
        }
    }}